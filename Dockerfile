FROM rust:latest

RUN rustup component add rustfmt clippy

# https://gitlab.com/dlalic/gitlab-clippy
RUN cargo install gitlab_clippy

# Coverage tools
RUN rustup component add llvm-tools-preview
RUN cargo install grcov
RUN apt-get update && apt-get install -y lcov python3-pip
RUN pip3 install lcov_cobertura

# Cross compile to windows
RUN cargo install cargo-c
RUN apt-get install -y mingw-w64
RUN rustup target add x86_64-pc-windows-gnu
RUN rustup toolchain install stable-x86_64-pc-windows-gnu
