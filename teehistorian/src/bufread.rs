use std::io::Read;

pub trait ThBufRead {
    /// returns current buffer
    fn get_buf(&self) -> &[u8];

    /// returns the number of extra bytes read, 0 indicates eof
    fn fill_buf(&mut self) -> std::io::Result<usize>;

    /// number of bytes to discard from the buffer. must be no more than the buffer len from get_buf
    fn consume(&mut self, amount: usize);
}

pub struct ThBufReader<R> {
    inner: R,
    buf: Vec<u8>,
    pos: usize,
    cap: usize,
}

impl<R: Read> ThBufReader<R> {
    pub fn new(inner: R) -> ThBufReader<R> {
        ThBufReader {
            inner,
            buf: vec![0; 4096],
            pos: 0,
            cap: 0,
        }
    }

    pub fn reuse(mut self, inner: R) -> Self {
        self.inner = inner;
        self.pos = 0;
        self.cap = 0;
        self
    }
}

impl<R: Read> ThBufRead for ThBufReader<R> {
    fn get_buf(&self) -> &[u8] {
        &self.buf[self.pos..self.cap]
    }

    fn fill_buf(&mut self) -> std::io::Result<usize> {
        let resize = if self.buf.len() / 2 < self.pos && self.buf.len() < self.cap + 8196 {
            // move buffer to front
            if self.pos < self.cap {
                self.buf.copy_within(self.pos..self.cap, 0);
                self.cap -= self.pos;
                self.pos = 0;
            }
            true
        } else {
            false
        };
        // If we've reached the end of our internal buffer then we need to fetch
        // some more data from the underlying reader.
        // Branch using `>=` instead of the more correct `==`
        // to tell the compiler that the pos..cap slice is always valid.
        if self.pos >= self.cap {
            debug_assert!(self.pos == self.cap);
            self.cap = 0;
            self.pos = 0;
        }
        if (resize && self.buf.capacity() < 67108864) || self.buf.len() < self.cap + 8196 {
            // 2^26 = 64M
            self.buf.resize(self.buf.capacity() * 2, 0);
        }

        let num_read = self.inner.read(&mut self.buf[self.cap..])?;
        self.cap += num_read;

        Ok(num_read)
    }

    fn consume(&mut self, amount: usize) {
        self.pos = std::cmp::min(self.pos + amount, self.cap);
    }
}

impl ThBufRead for &[u8] {
    fn get_buf(&self) -> &[u8] {
        self
    }

    fn fill_buf(&mut self) -> std::io::Result<usize> {
        Ok(0)
    }

    fn consume(&mut self, amount: usize) {
        *self = &self[amount..];
    }
}
