use crate::error::ErrorKind;
use std::ffi::CStr;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::Write;
use std::os::raw::c_char;

use crate::ThBufReader;

enum Error {
    None,
    Eof,
    ParseError(ErrorKind),
    IoError(io::Error),
    Utf8Error(std::str::Utf8Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::None => write!(f, "No detailed info available"),
            Error::Eof => write!(f, "End of file"),
            Error::ParseError(err) => write!(f, "ParseError {}", err),
            Error::IoError(err) => write!(f, "IoError {}", err),
            Error::Utf8Error(err) => write!(f, "Utf8Error {}", err),
        }
    }
}

struct LastReturn {
    value: ThResult,
    /// detailed error struct containing last error
    error: Error,
}

pub struct Teehistorian {
    teehistorian: Option<crate::Th<ThBufReader<File>>>,
    last_chunk: Option<ChunkRust<'static>>,
    last_c_chunk: Option<Chunk>,
    last_return: LastReturn,

    error_str: Vec<u8>,
    args: Vec<*const c_char>,
    /// print of Chunk
    chunk_str: Vec<u8>,
}

#[repr(C)]
#[derive(Debug)]
pub enum Chunk {
    PlayerDiff {
        /// integer in range: 0 <= cid < 64
        cid: i32,
        dx: i32,
        dy: i32,
    },
    /// End of teehistorian chunk stream
    Eos,
    TickSkip {
        /// should be >= 0
        dt: i32,
    },
    PlayerNew {
        cid: i32,
        x: i32,
        y: i32,
    },
    PlayerOld {
        cid: i32,
    },
    InputDiff {
        cid: i32,
        /// 10 integers
        dinput: [i32; 10],
    },
    InputNew {
        cid: i32,
        /// 10 integers
        input: [i32; 10],
    },
    NetMessage {
        cid: i32,
        size: i32,
        msg: *const c_char,
    },
    Join {
        cid: i32,
    },
    Drop {
        cid: i32,
        reason: *const c_char,
    },
    ConsoleCommand {
        cid: i32,
        flags: i32,
        cmd: *const c_char,
        num_args: i32,
        args: *const *const c_char,
    },
    UnknownEx {
        uuid: [u8; 16],
        size: i32,
        data: *const u8,
    },
    Test,
    DdnetverOld {
        cid: i32,
        version: i32,
    },
    Ddnetver {
        cid: i32,
        connection_id: [u8; 16],
        version: i32,
        version_str: *const c_char,
    },
    AuthInit {
        cid: i32,
        level: i32,
        auth_name: *const c_char,
    },
    AuthLogin {
        cid: i32,
        level: i32,
        auth_name: *const c_char,
    },
    AuthLogout {
        cid: i32,
    },
    JoinVer6 {
        cid: i32,
    },
    JoinVer7 {
        cid: i32,
    },
    RejoinVer6 {
        cid: i32,
    },
    TeamSaveSuccess {
        team: i32,
        save_id: [u8; 16],
        save: *const c_char,
    },
    TeamSaveFailure {
        team: i32,
    },
    TeamLoadSuccess {
        team: i32,
        save_id: [u8; 16],
        save: *const c_char,
    },
    TeamLoadFailure {
        team: i32,
    },
    PlayerTeam {
        cid: i32,
        team: i32,
    },
    TeamPractice {
        team: i32,
        practice: i32,
    },
    PlayerReady {
        cid: i32,
    },
    PlayerSwap {
        cid1: i32,
        cid2: i32,
    },
    Antibot {
        data_len: i32,
        data: *const c_char,
    },
    PlayerName {
        cid: i32,
        name: *const c_char,
    },
    PlayerFinish {
        cid: i32,
        time: i32,
    },
    TeamFinish {
        team: i32,
        time: i32,
    },
}

use crate::chunks::Chunk as ChunkRust;

impl Teehistorian {
    fn convert_chunk(chunk: &ChunkRust, args_buf: &mut Vec<*const c_char>) -> Chunk {
        match chunk {
            ChunkRust::PlayerDiff(p) => Chunk::PlayerDiff {
                cid: p.cid,
                dx: p.dx,
                dy: p.dy,
            },
            ChunkRust::Eos => Chunk::Eos,
            ChunkRust::TickSkip(t) => Chunk::TickSkip { dt: t.dt },
            ChunkRust::PlayerNew(p) => Chunk::PlayerNew {
                cid: p.cid,
                x: p.x,
                y: p.y,
            },
            ChunkRust::PlayerOld(p) => Chunk::PlayerOld { cid: p.cid },
            ChunkRust::InputDiff(i) => Chunk::InputDiff {
                cid: i.cid,
                dinput: i.dinput.clone(),
            },
            ChunkRust::InputNew(i) => Chunk::InputNew {
                cid: i.cid,
                input: i.input.clone(),
            },
            ChunkRust::NetMessage(n) => Chunk::NetMessage {
                cid: n.cid,
                size: n.msg.len() as i32,
                msg: n.msg.as_ptr() as *const c_char,
            },
            ChunkRust::Join(p) => Chunk::Join { cid: p.cid },
            ChunkRust::Drop(p) => Chunk::Drop {
                cid: p.cid,
                reason: p.reason.as_ptr() as *const c_char,
            },
            ChunkRust::ConsoleCommand(c) => {
                *args_buf = c.args.iter().map(|s| s.as_ptr() as *const c_char).collect();
                Chunk::ConsoleCommand {
                    cid: c.cid,
                    flags: c.flags,
                    cmd: c.cmd.as_ptr() as *const c_char,
                    num_args: c.num_args,
                    args: args_buf.as_ptr(),
                }
            }
            ChunkRust::UnknownEx(e) => Chunk::UnknownEx {
                uuid: *e.uuid.as_bytes(),
                size: e.size,
                data: e.data.as_ptr(),
            },
            ChunkRust::Test => Chunk::Test,
            ChunkRust::DdnetVersionOld(v) => Chunk::DdnetverOld {
                cid: v.cid,
                version: v.version,
            },
            ChunkRust::DdnetVersion(v) => Chunk::Ddnetver {
                cid: v.cid,
                connection_id: *v.connection_id.as_bytes(),
                version: v.version,
                version_str: v.version_str.as_ptr() as *const c_char,
            },
            ChunkRust::AuthInit(a) => Chunk::AuthInit {
                cid: a.cid,
                level: a.level,
                auth_name: a.auth_name.as_ptr() as *const c_char,
            },
            ChunkRust::AuthLogin(a) => Chunk::AuthLogin {
                cid: a.cid,
                level: a.level,
                auth_name: a.auth_name.as_ptr() as *const c_char,
            },
            ChunkRust::AuthLogout(a) => Chunk::AuthLogout { cid: a.cid },
            ChunkRust::JoinVer6(p) => Chunk::JoinVer6 { cid: p.cid },
            ChunkRust::JoinVer7(p) => Chunk::JoinVer7 { cid: p.cid },
            ChunkRust::RejoinVer6(p) => Chunk::RejoinVer6 { cid: p.cid },
            ChunkRust::TeamSaveSuccess(s) => Chunk::TeamSaveSuccess {
                team: s.team,
                save_id: *s.save_id.as_bytes(),
                save: s.save.as_ptr() as *const c_char,
            },
            ChunkRust::TeamSaveFailure(s) => Chunk::TeamSaveFailure { team: s.team },
            ChunkRust::TeamLoadSuccess(s) => Chunk::TeamLoadSuccess {
                team: s.team,
                save_id: *s.save_id.as_bytes(),
                save: s.save.as_ptr() as *const c_char,
            },
            ChunkRust::TeamLoadFailure(s) => Chunk::TeamLoadFailure { team: s.team },
            ChunkRust::PlayerTeam(pt) => Chunk::PlayerTeam {
                cid: pt.cid,
                team: pt.team,
            },
            ChunkRust::TeamPractice(tp) => Chunk::TeamPractice {
                team: tp.team,
                practice: tp.practice,
            },
            ChunkRust::PlayerReady(p) => Chunk::PlayerReady { cid: p.cid },
            ChunkRust::PlayerSwap(s) => Chunk::PlayerSwap {
                cid1: s.cid1,
                cid2: s.cid2,
            },
            ChunkRust::Antibot(a) => Chunk::Antibot {
                data_len: a.data.len() as i32,
                data: a.data.as_ptr() as *const c_char,
            },
            ChunkRust::PlayerName(p) => Chunk::PlayerName {
                cid: p.cid,
                name: p.name.as_ptr() as *const c_char,
            },
            ChunkRust::PlayerFinish(p) => Chunk::PlayerFinish {
                cid: p.cid,
                time: p.time,
            },
            ChunkRust::TeamFinish(t) => Chunk::TeamFinish {
                team: t.team,
                time: t.time,
            },
        }
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
#[repr(C)]
/// cbindgen:rename-all=ScreamingSnakeCase
pub enum ThResult {
    /// The TH_OK result code means that the operation was successful and that there were no errors.
    /// All other result codes indicate an error.
    ThOk = 0,
    /// The TH_IO_ERROR result code indicates that an error occurred while accessing the
    /// teehistorian file.
    ThIoError = 1,

    ThParseError = 2,
    /// The TH_MISUSE result code might be returned if the application uses any Teehistorian
    /// interface in a way that is undefined or unsupported.
    ThMisuse = 3,
    /// No further chunk can be parsed
    ThEof,
}

impl LastReturn {
    fn handle_error(&mut self, error: crate::Error) -> ThResult {
        self.value = match error {
            crate::Error::Message(_) => ThResult::ThParseError,
            crate::Error::ParseError(err) => {
                self.error = Error::ParseError(err);
                ThResult::ThParseError
            }
            crate::Error::IoError(err) => {
                self.error = Error::IoError(err);
                ThResult::ThIoError
            }
            crate::Error::Eof => {
                self.error = Error::Eof;
                ThResult::ThEof
            }
        };
        self.value
    }

    fn on_return(&mut self, return_value: ThResult, error: Error) -> ThResult {
        self.error = error;
        self.value = return_value;
        return return_value;
    }
}

/// Opening A Teehistorian File
///
/// Whether or not an error occurs when it is opened, resources associated with the teehistorian
/// file should be released by passing it to `th_close`.
#[no_mangle]
pub extern "C" fn th_open(filename: *const c_char, th: &mut *mut Teehistorian) -> ThResult {
    let new_th = Box::new(Teehistorian {
        teehistorian: None,
        last_chunk: None,
        last_c_chunk: None,
        last_return: LastReturn {
            value: ThResult::ThOk,
            error: Error::None,
        },
        error_str: Vec::new(),
        args: Vec::new(),
        chunk_str: Vec::new(),
    });
    let new_th = Box::leak(new_th);
    *th = new_th;
    let filename = unsafe { CStr::from_ptr(filename as *const i8) };
    let filename = match filename.to_str() {
        Ok(f) => f,
        Err(err) => {
            return new_th
                .last_return
                .on_return(ThResult::ThIoError, Error::Utf8Error(err))
        }
    };
    let f = match File::open(filename) {
        Ok(f) => f,
        Err(err) => {
            return new_th
                .last_return
                .on_return(ThResult::ThIoError, Error::IoError(err))
        }
    };
    match super::Th::parse(ThBufReader::new(f)) {
        Ok(th) => new_th.teehistorian = Some(th),
        Err(err) => return new_th.last_return.handle_error(err),
    }
    ThResult::ThOk
}

#[no_mangle]
/// After the the teehistorian file has been successfully opened, `th_header` can be used to
/// retrieve the teehistorian header json as a string. This function can only be called until
/// `th_step()` has been called once
pub extern "C" fn th_header(th: &mut Teehistorian, header: &mut *const c_char) -> ThResult {
    match th.teehistorian.as_mut() {
        Some(parser) => match parser.header() {
            Ok(h) => {
                *header = h.as_ptr() as *const c_char;
                return th.last_return.on_return(ThResult::ThOk, Error::None);
            }
            Err(err) => {
                return th.last_return.handle_error(err);
            }
        },
        None => {
            *header = 0 as *const c_char;
            return th.last_return.on_return(ThResult::ThMisuse, Error::None);
        }
    }
}

#[no_mangle]
/// After the the teehistorian file has been successfully opened, `th_step` can be used to access
/// all chunks. the chunk can only be accessed if `TH_OK` is returned
pub extern "C" fn th_step(th: &'static mut Teehistorian, chunk: &mut *const Chunk) -> ThResult {
    th.chunk_str.clear();
    match th.teehistorian.as_mut() {
        Some(parser) => match parser.next_chunk() {
            Ok(rust_chunk) => {
                th.last_c_chunk = Some(Teehistorian::convert_chunk(&rust_chunk, &mut th.args));
                th.last_chunk = Some(rust_chunk);
                *chunk = th.last_c_chunk.as_ref().unwrap();
                return th.last_return.on_return(ThResult::ThOk, Error::None);
            }
            Err(err) => return th.last_return.handle_error(err),
        },
        None => {
            return th.last_return.on_return(ThResult::ThMisuse, Error::None);
        }
    };
}

/// Closing A Teehistorian file
///
/// The `th_close()` is a the destructor for the Teehistorian object. All associated resources are
/// deallocated.
#[no_mangle]
pub extern "C" fn th_close(th: *mut Teehistorian) {
    unsafe {
        let _ = Box::from_raw(th);
    }
}

/// Error Messages
///
/// The `th_errmsg()` returns the English-language text that describes the result code, as UTF-8.
/// Memory to hold the error message string is managed internally and is invalidated by the next
/// call to either `th_step()`
#[no_mangle]
pub extern "C" fn th_errmsg(th: &mut Teehistorian) -> *const c_char {
    use ThResult::*;
    th.error_str.clear();
    match th.last_return.value {
        ThOk => write!(th.error_str, "No error occured\0").unwrap(),
        ThEof => write!(th.error_str, "Reached end of file\0").unwrap(),
        ThIoError => write!(
            th.error_str,
            "Failed to open file: {}\0",
            th.last_return.error
        )
        .unwrap(),
        ThParseError => write!(
            th.error_str,
            "Error parsing file: {}\0",
            th.last_return.error
        )
        .unwrap(),
        ThMisuse => write!(th.error_str, "Misuse: {}\0", th.last_return.error).unwrap(),
    }
    th.error_str.as_ptr() as *const c_char
}

/// Retrieving Chunk String
///
/// Returns string describing the content of the last returned chunk from `th_step()`
/// Memory to hold the error message string is managed internally and is invalidated by the next
/// call to either `th_step()`
#[no_mangle]
pub extern "C" fn th_chunk_desc(th: &mut Teehistorian) -> *const c_char {
    if th.chunk_str.len() == 0 {
        if let Some(chunk) = th.last_chunk.as_ref() {
            write!(th.chunk_str, "{:?}\0", chunk).unwrap();
        } else {
            write!(th.chunk_str, "\0").unwrap();
        }
    }
    th.chunk_str.as_ptr() as *const c_char
}
