#include "teehistorian.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("usage: %s TEEHISTORIAN_FILE\n", argv[0]);
		return 1;
	}
	Teehistorian *pTh;
	if(TH_OK != th_open(argv[1], &pTh)) {
		printf("%s\n", th_errmsg(pTh));
		th_close(pTh);
		return 1;
	}

	const Chunk *pChunk;
	while(1) {
		if(TH_OK != th_step(pTh, &pChunk)) {
			printf("%s\n", th_errmsg(pTh));
			th_close(pTh);
			return 1;
		}
		printf("%s\n", th_chunk_desc(pTh));
		if(pChunk->tag == Eos) {
			th_close(pTh);
			return 0;
		}
	}
}
