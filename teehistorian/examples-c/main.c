#include "teehistorian.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("usage: %s TEEHISTORIAN_FILE\n", argv[0]);
		return 1;
	}
	Teehistorian *pTh;
	if(TH_OK != th_open(argv[1], &pTh)) {
		printf("%s\n", th_errmsg(pTh));
		th_close(pTh);
		return 1;
	}

	const Chunk *pChunk;
	while(1) {
		if(TH_OK != th_step(pTh, &pChunk)) {
			printf("%s\n", th_errmsg(pTh));
			th_close(pTh);
			return 1;
		}
		switch(pChunk->tag) {
		case PlayerDiff:
			printf("PlayerDiff { cid: %i, x: %i, y: %i }\n",
					pChunk->player_diff.cid,
					pChunk->player_diff.dx,
					pChunk->player_diff.dy);
			break;
		case Eos:
			printf("Eos\n");
			th_close(pTh);
			return 0;
		case TickSkip:
			printf("TickSkip { dt: %i }", pChunk->tick_skip.dt);
			break;
		case PlayerNew:
			break;
		case PlayerOld:
			break;
		case InputDiff:
			break;
		case InputNew:
			break;
		case NetMessage:
			break;
		case Join:
			printf("Join { cid: %i }\n", pChunk->join.cid);
			break;
		case Drop:
			printf("Drop { cid: %i, reason: \"%s\" }\n", pChunk->drop.cid, pChunk->drop.reason);
			break;
		case ConsoleCommand:
			break;
		case UnknownEx:
			break;
		}
	}
}
