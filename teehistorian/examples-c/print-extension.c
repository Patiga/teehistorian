#include "teehistorian.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("usage: %s TEEHISTORIAN_FILE\n", argv[0]);
		return 1;
	}
	Teehistorian *pTh;
	if(TH_OK != th_open(argv[1], &pTh)) {
		printf("%s\n", th_errmsg(pTh));
		th_close(pTh);
		return 1;
	}

	const Chunk *pChunk;
	while(1) {
		if(TH_OK != th_step(pTh, &pChunk)) {
			printf("%s\n", th_errmsg(pTh));
			th_close(pTh);
			return 1;
		}
		switch(pChunk->tag) {
		case Eos:
			return 0;
		case DdnetverOld:
		case Ddnetver:
		case AuthInit:
		case AuthLogin:
		case AuthLogout:
		case JoinVer6:
		case JoinVer7:
		case UnknownEx:
		case Test:
			printf("%s\n", th_chunk_desc(pTh));
			break;
		default:
			break;
		}
	}
}
