#!/usr/bin/env python3
import sys

def main():
	if len(sys.argv) != 2:
		print('usage: ' + sys.argv[0] + ' vx.y.z')
		return
	version = sys.argv[1]
	
	print('https://gitlab.com/zwelf/teehistorian/-/jobs/artifacts/{}/download?job=build%3Acargo'.format(version))
	print('Linux binaries and header')
	print()
	print('https://gitlab.com/zwelf/teehistorian/-/jobs/artifacts/{}/download?job=build%3Awindows'.format(version))
	print('Windows binaries and header')

if __name__ == '__main__':
	main()
