# Teehistorian parser

This is a Teehistorian parser, a data format for DDNet servers saving all input to reproduce it faithfully.

## C-API

Look into the `teehistorian.h` from the [release page](https://gitlab.com/zwelf/teehistorian/-/releases) or into the [examples](examples-c) for a usage overview.

Using [cargo-c](https://github.com/lu-zero/cargo-c) to build c/c++ static and dynamic libraries.
You can install it using `cargo install cargo-c`.

The library can be compiled using

```bash
cargo cbuild
```

## Running the C example

```bash
$ cargo cbuild --release
$ cp target/release/libteehistorian.so target/release/teehistorian.h examples-c/
$ cd examples-c/
$ gcc -L. -l:libteehistorian.so print-all.c -o print-all.out
$ ln -s libteehistorian.so libteehistorian.so.0
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:. ./print-all.out ../tests/minimal.teehistorian
Eos
```

## License

[LGPL-3.0](LICENSE)
